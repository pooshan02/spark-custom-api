package com.talentica.spark.dataframe

import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.IntegerType

object DataframeApp {
  def main(args: Array[String]) {

    val config = new SparkConf().setMaster("local[*]").setAppName("Custom Dataframe")

    val sc = new SparkContext(config)
    sc.setLogLevel("WARN")
    val sqlContext = new SQLContext(sc)

    val schema = StructType(Seq(StructField("name", StringType, nullable = true), StructField("age", IntegerType, nullable = true)))

    val path = getClass.getResource("/person.csv").getPath

    val df = sqlContext.read.format("com.talentica.spark.dataframe").schema(schema).load(path)

    df.printSchema()
    df.show() // show all the columns of the table

    df.createOrReplaceTempView("person")
    val df1 = sqlContext.sql("Select name from person")
    df1.show() // Show only selected columns and pruning is executed

  }
}
