package com.talentica.spark.dataframe

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.sources.BaseRelation
import org.apache.spark.sql.sources.PrunedScan
import org.apache.spark.sql.sources.TableScan
import org.apache.spark.sql.types._
import org.apache.spark.sql.types.IntegerType

class CustomBaseRelation(location: String, userSchema: StructType)(@transient val sqlContext: SQLContext)
    extends BaseRelation
    with TableScan with PrunedScan with Serializable {

  override def buildScan(requiredColumns: Array[String]): RDD[Row] = {
    val schemaFields = schema.fields
    val rdd = sqlContext.sparkContext.wholeTextFiles(location).map(f => f._2)
    val rows = rdd.map(file => {
      val lines = file.split("\n")
      val data = lines.map(line => line.split(",").map(col => col.trim()).seq)
      val temp = data.map(words => words.zipWithIndex.map {
        case (value, index) => {
          val name = schemaFields(index).name
          if (requiredColumns.contains(name))
            Some(castValue(value, schemaFields(index).dataType))
          else None
        }
      })
      temp.map(s => Row.fromSeq(s.filter(_.isDefined).map(value => value.get)))
    })
    rows.flatMap(e => e)
  }

  override def schema: StructType = {
    if (this.userSchema != null) {
      return this.userSchema
    } else {
      return null
    }
  }

  private def castValue(value: String, toType: DataType): Any = toType match {
    case _: StringType  => value.toString()
    case _: IntegerType => value.toInt
    case _: DoubleType  => value.toDouble
    case _: LongType    => value.toLong
    case _: FloatType   => value.toFloat
  }

  override def buildScan(): RDD[Row] = {
    val schemaFields = schema.fields
    val rdd = sqlContext.sparkContext.wholeTextFiles(location).map(f => f._2)
    val rows = rdd.map(file => {
      val lines = file.split("\n")
      val data = lines.map(line => line.split(",").map(col => col.trim()).seq)
      val temp = data.map(words => words.zipWithIndex.map {
        case (value, index) => {
          val name = schemaFields(index).name
          Some(castValue(value, schemaFields(index).dataType))
        }
      })
      temp.map(s => Row.fromSeq(s.filter(_.isDefined).map(value => value.get)))
    })
    rows.flatMap(e => e)
  }
}

object CustomBaseRelation {
  def apply(location: String, userSchema: StructType)(sqlContext: SQLContext): CustomBaseRelation =
    return new CustomBaseRelation(location, userSchema)(sqlContext)
}