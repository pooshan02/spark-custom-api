package com.talentica.spark.dataframe

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{ Row, DataFrame, SaveMode, SQLContext }
import org.apache.spark.sql.sources.{ CreatableRelationProvider, BaseRelation, RelationProvider, SchemaRelationProvider }
import org.apache.spark.sql.types._

class DefaultSource extends RelationProvider with SchemaRelationProvider {
  override def createRelation(sqlContext: SQLContext, parameters: Map[String, String]): BaseRelation = {
    createRelation(sqlContext, parameters, null)
  }

  override def createRelation(sqlContext: SQLContext, parameters: Map[String, String], schema: StructType): BaseRelation = {
    parameters.getOrElse("path", sys.error("'path' must be specified."))

    return CustomBaseRelation(parameters.get("path").get, schema)(sqlContext)
  }

  def saveAsCsvFile(data: DataFrame, path: String) = {
    val dataCustomIterator = data.rdd.map(row => {
      val values = row.toSeq.map(value => value.toString)
      values.mkString(",")
    })
    dataCustomIterator.saveAsTextFile(path)
  }

}
  
