package com.talentica.spark.rdd

import org.apache.spark.SparkContext

import CustomFunctions.addCustomFunctions
import scala.io.Source

object RDDApp extends App {
  val sc = new SparkContext("local[*]", "Custom RDD ")
  
  val path = getClass.getResource("/sales.csv").getPath
  
  val dataRDD = sc.textFile(path)
  val salesRecordRDD = dataRDD.map(row => {
    val colValues = row.split(",")
    new SalesRecord(colValues(0), colValues(1), colValues(2), colValues(3).toDouble)
  })

  println("Original list of items and values")
  salesRecordRDD.collect().foreach { x => println(x) }

  println("Total sales :: " + salesRecordRDD.totalSales)

  // discount RDD
  val discountRDD = salesRecordRDD.discount(10)
  println("10 % discount on each items")
  discountRDD.collect().foreach { x => println(x) }
}