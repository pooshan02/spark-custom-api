package com.talentica.spark.rdd

import org.apache.spark.Partition
import org.apache.spark.TaskContext
import java.lang.Double
import org.apache.spark.rdd.RDD

class DiscountRDD(prev:RDD[SalesRecord],discountPercentage:Double) extends RDD[SalesRecord](prev){
  override def compute(split: Partition, context: TaskContext): Iterator[SalesRecord] =  {
    firstParent[SalesRecord].iterator(split, context).map(salesRecord => {
     val discount  = salesRecord.itemValue*discountPercentage /100
      new SalesRecord(salesRecord.transactionId,salesRecord.customerId,salesRecord.itemId,discount)
    })}

  override protected def getPartitions: Array[Partition] = firstParent[SalesRecord].partitions
}