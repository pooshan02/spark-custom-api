# README #
========================  
  EXTENDING SPARK API  
========================  
### What is this repository for? ###

This repository mainly consist of the custom implementation of the Spark API such as RDD and Datasource.  

### How do I get set up? ###

The setup is very simple and easy. You have to just checkout the project and run "gradle clean build".  

### How to run the application? ###
1. For *Custom RDD* : You will have to run the application "RDDApp".  
2. For *Custom Dataframe* : You will have to run the application "DataframeApp".  


